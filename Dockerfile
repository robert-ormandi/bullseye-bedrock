FROM amd64/debian:bullseye-slim

# Install the basic packages from the default repository.
RUN apt-get --yes update && \
    apt-get --yes --no-install-recommends install \
        curl \
        locales \
        python3 \
        python3-pip \
        python3-venv \
        python3-dev \
        unzip

# Set Python 3 as a Python alternative.
RUN update-alternatives --install /usr/bin/python python \
    /usr/bin/python3 1

# Set pip3 as a pip alternative.
RUN update-alternatives --install /usr/bin/pip pip \
    /usr/bin/pip3 1

# Set locale to en_US.UTF-8.
RUN rm -rf /var/lib/apt/lists/* && \
    localedef -i en_US -c -f UTF-8 \
        -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

# Set working directory
ENV WORK_DIR="/minecraft_server/"

WORKDIR $WORK_DIR

# Download and uncompress the Minecraft server.
ENV VERSION="1.16.201.02"
ENV SERVER_COMPRESSED="bedrock-server-${VERSION}.zip"
ENV DOWNLOAD_URL="https://minecraft.azureedge.net/bin-linux/${SERVER_COMPRESSED}"

ADD ${DOWNLOAD_URL} ${SERVER_COMPRESSED}
RUN unzip -qq ${SERVER_COMPRESSED}
# Remove the compressed server package to minimize the size of the Docker image.
RUN rm ${SERVER_COMPRESSED}

# Make sure that the directory `worlds` exists.
RUN mkdir worlds

# Delete the original server configurations. Add the prepared ones (this may
# not be backward compatible).
RUN rm server.properties permissions.json whitelist.json
ADD server.properties permissions.json whitelist.json ${WORK_DIR}

# Expose the server port (UDP).
EXPOSE 19132

# Start the server.
ENV PATH=${PATH}:${WORK_DIR}
ENV LD_LIBRARY_PATH="."

ENTRYPOINT bedrock_server
