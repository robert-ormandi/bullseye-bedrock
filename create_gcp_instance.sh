#!/bin/bash

export GCP_PROJECT="minecraft-server-20201222"

# Create a fix external IP for the server.
gcloud compute addresses create minecraft-bedrock-address --region us-west1

# Create instance.
gcloud compute --project=${GCP_PROJECT} instances \
       create-with-container minecraft-bedrock-vm \
       --container-image docker.io/rormandi/bullseye-bedrock:latest \
       --create-disk name=minecraft-server-worlds,size=5GB \
       --address minecraft-bedrock-address \
       --container-mount-disk mount-path="/minecraft_server/worlds",name=minecraft-server-worlds,mode=rw \
       --tags minecraft-server \
       --zone=us-west1-b \
       --machine-type=e2-medium

# Allow UDP port 19132 on firewall.
gcloud compute --project=${GCP_PROJECT} firewall-rules create allow-minecraft-port \
       --description="Bedrock server uses UPD port 19132. Allow this externally." \
       --target-tags minecraft-server \
       --action=ALLOW \
       --rules=udp:19132 \
       --source-ranges=0.0.0.0/0
