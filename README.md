# Bedrock Minecraft Server in Docker

See attached a Debian Bullseye (slim)-based `Dockerfile` which:

  * Downloads the Minecraft Bedrock server version `1.16.201.02`,
  * Adds the predefined server config and user allow list, and
  * Starts the server.

The lates version of the created image is already pushed to Docker Hub:
https://hub.docker.com/r/rormandi/bullseye-bedrock

Assumed installed Docker the server can be started by:
`docker run --publish 19132:19132/udp --name bedrock rormandi/bullseye-bedrock:latest`

In order to connect from PS 4 an UDP proxy is needed e.g.:
https://github.com/jhead/phantom

See the script `create_gcp_instance.sh` to create a new GCP COS instance
running the `latest` container image and sets up the firewall.

User restrictions: Only Dani, Mark, Moci and Zsofica can play.
