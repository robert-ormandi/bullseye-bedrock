#!/bin/bash

export GCP_PROJECT="minecraft-server-20201222"
export UPDATED_IMAGE_TAG="latest"

# Note: This operation restarts the VM, so the IP may change!

# Push new image to Docker Hub with the tag ${UPDATED_IMAGE_TAG} first, then:
gcloud beta compute --project=${GCP_PROJECT} instances \
       update-container minecraft-bedrock-vm \
       --container-image docker.io/rormandi/bullseye-bedrock:${UPDATED_IMAGE_TAG} \
       --zone us-west1-b
